import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {FormsModule} from '@angular/forms'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http'; 
import { LoginModule } from './modules/login/login.module';
import { APP_BASE_HREF, LocationStrategy, HashLocationStrategy } from '@angular/common';
import { JwtModule } from '@auth0/angular-jwt';
import { DashboardComponent } from './modules/dashboard/pages/dashboard/dashboard.component';
import { DashboardModule } from './modules/dashboard/dashboard.module';
import { UserModule } from './modules/user/user.module';
import { SharedModule } from './shared/shared.module';
import { ReportsModule } from './modules/reports/reports.module';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NgxSpinnerComponent } from './shared/components/ngx-spinner/ngx-spinner.component';
import { AddItemsRoutes } from './modules/add-items/add-items-routing.module';
import { AddItemsModule } from './modules/add-items/add-items.module';
import { SelectItemsModule } from './modules/select-items/select-items.module';

export function tokenGetter() {
  return localStorage.getItem('access_token');
}

@NgModule({
  declarations: [
    AppComponent,
    // DashboardComponent,
    NgxSpinnerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    LoginModule,
    UserModule,
    ReportsModule,
    AddItemsModule,
    SelectItemsModule,
    DashboardModule,
    NgxSpinnerModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        whitelistedDomains: ['localhost:8080','0.0.0.0:8080','103.69.125.117:8080','192.168.10.78:8080'],
        authScheme: ''
        //blacklistedRoutes: ['localhost/login']
      }
    }),
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
],
  providers: [
    { provide: APP_BASE_HREF, useValue: '/' },
    { provide: LocationStrategy, useClass: HashLocationStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
