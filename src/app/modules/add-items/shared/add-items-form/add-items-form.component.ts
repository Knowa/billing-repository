import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { User } from 'app/shared/models/user.model';
import { UserService } from 'app/modules/user/services/user.service';

@Component({
  selector: 'billing-add-items-form',
  templateUrl: './add-items-form.component.html',
  styleUrls: ['./add-items-form.component.scss']
})
export class AddItemsFormComponent implements OnInit {

  user:User = new User();

  title="Add Items";
  selectedFile: any = File;


  public imagePath;
  imgURL: any;
  public message: string;

  constructor(
    private modal: NgbActiveModal,
    private userService:UserService
  ) { }

  ngOnInit() {
  }

  cancel(){
    this.modal.dismiss();
  }

  onCancel(){
    this.cancel();
  }
  onSave(){
   
      this.userService.getAdmin()
        .subscribe(
          data => {
            console.log(data);
            
          },
          error =>{
            console.log(error);
            
          }
        )
      
    
  
    
  }
  
  selectFile(event) {
    const file = event.target.files[0];
    this.selectedFile = file;
    console.log(this.selectedFile);
    


    //reding file detail to get image url for preview purpose
    var reader = new FileReader();
    this.imagePath = this.selectedFile;
    reader.readAsDataURL(this.selectedFile); 
    reader.onload = (_event) => { 
      this.imgURL = reader.result; 
    }
  }



}
