import { Component, OnInit } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AddItemsFormComponent } from '../../shared/add-items-form/add-items-form.component';

@Component({
  selector: 'billing-add-items',
  templateUrl: './add-items.component.html',
  styleUrls: ['./add-items.component.scss']
})
export class AddItemsComponent implements OnInit {

  constructor(
    private modal: NgbModal,

  ) { }

  itemList =[
    { 'price':100,
      'name': 'momo',
      'image': ''
    },
    {
      'price':1101,
      'name': 'pizza',
      'image': ''
    },
    {
      'price':2000,
      'name': 'burger',
      'image': ''
    },
    {
      'price':1500,
      'name': 'wine',
      'image': ''
    }
  ]

  ngOnInit() {
  }

  showItemsPopupForm(mode){
    if(mode=='add'){

      const modal: NgbModalRef = this.modal.open(AddItemsFormComponent, { size: 'sm' , backdrop: 'static'});
    }

  }

}
