import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddItemsComponent } from './pages/add-items/add-items.component';


export const AddItemsRoutes: Routes = [
  {
    path:'',
    component:AddItemsComponent

  }
];

@NgModule({
  imports: [RouterModule.forChild(AddItemsRoutes)],
  exports: [RouterModule]
})
export class AddItemsRoutingModule { }
