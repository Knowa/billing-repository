import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddItemsRoutingModule } from './add-items-routing.module';
import { AddItemsComponent } from './pages/add-items/add-items.component';
import { AddItemsFormComponent } from './shared/add-items-form/add-items-form.component';
import { SharedModule } from 'app/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { UserService } from '../user/services/user.service';


@NgModule({
  declarations: [AddItemsComponent, AddItemsFormComponent],
  imports: [
    SharedModule,
    CommonModule,
    AddItemsRoutingModule,
    FormsModule
    
  ],
  entryComponents:[AddItemsFormComponent],
  providers:[UserService]
})
export class AddItemsModule { }
