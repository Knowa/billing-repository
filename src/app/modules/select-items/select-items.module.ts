import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SelectItemsRoutingModule } from './select-items-routing.module';
import { SelectItemsComponent } from './pages/select-items/select-items.component';
import { SharedModule } from 'app/shared/shared.module';
import { BillConfirmComponent } from './pages/bill-confirm/bill-confirm.component';
import { FinalBillComponent } from './pages/final-bill/final-bill.component';


@NgModule({
  declarations: [SelectItemsComponent, BillConfirmComponent, FinalBillComponent],
  imports: [
    SharedModule,
    CommonModule,
    SelectItemsRoutingModule,
  ],
  entryComponents:[
    BillConfirmComponent
  ]
  
})
export class SelectItemsModule { }
