import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'billing-bill-confirm',
  templateUrl: './bill-confirm.component.html',
  styleUrls: ['./bill-confirm.component.scss']
})
export class BillConfirmComponent implements OnInit {

  title = 'Confirm Message !';

  message = 'Are you sure you want to generate bill ?';

  constructor(
    private modal: NgbActiveModal
  ) { }

  close() {
    this.modal.close();
  }

  dismiss() {
    this.modal.dismiss();
  }

  ngOnInit() {
  }

}
