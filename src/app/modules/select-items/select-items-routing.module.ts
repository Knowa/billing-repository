import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SelectItemsComponent } from './pages/select-items/select-items.component';


 export const SelectItemRoutes: Routes = [
  {
  path:'',
  component: SelectItemsComponent
  },
  // {
  //   path:'bill',
  //   component: FinalBillComponent
  // }

];

@NgModule({
  imports: [RouterModule.forChild(SelectItemRoutes)],
  exports: [RouterModule]
})
export class SelectItemsRoutingModule { }
