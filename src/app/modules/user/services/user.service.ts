import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { User } from 'app/shared/models/user.model';
import { HttpResult } from 'app/shared/models/http-result.model';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private http: HttpClient
  ) { }


  // getUsers({ size, page, sort }): Observable<any> {
  //   const params = new HttpParams({
  //     fromObject: {
  //       size, page, sort
  //     }
  //   });
  //   return (
  //     this.http
  //       .get<any>('/users', { params })
  //       .pipe(
  //         catchError(err => {
  //           return Observable.throw(err);
  //         })
  //       )
  //   );
  // }



  getUsers(): Observable<any> {
    return (
      this.http
        .get<any>('/users')
        .pipe(
          catchError(err => {
            return Observable.throw(err);
          })
        )
    );
  }
  


  getAdmin(){
    return this.http.get('/admin');
  }


  addUser(user) {
    return (
      this.http
        .post<HttpResult>('/users', { ...user })
    );
  }

  updateUser(user: User, userId) {
    console.log(user);
    
    return (
      this.http
        .put<HttpResult>(`/users/${userId}`, { ...user })
    );
  }

  deleteUser(id) {
    return (
      this.http
        .delete<HttpResult>('/users/'+id)
        .pipe(
          catchError(err => {
            return Observable.throw(err);
          })
        )
    );
  }
}
