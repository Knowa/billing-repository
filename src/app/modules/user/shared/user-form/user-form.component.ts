import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { User } from 'app/shared/models/user.model';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {

  title="Add User";

  showPassword = false;
  mode ='';

  user: User = new User();


  constructor(
    private modal: NgbActiveModal,
    private spinner: NgxSpinnerService,
    private userService: UserService
  ) { }

  ngOnInit() {
    console.log(this.mode);
    
  
    
   
  }



  cancel(){
    this.modal.dismiss();
  }

  onCancel(){
    this.cancel();
  }

  onSave(){
    this.modal.close(Object.assign({}, this.user));
  }

}
