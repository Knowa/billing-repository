import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserFormComponent } from './shared/user-form/user-form.component';
import { UserComponent } from './pages/user/user.component';


export const UserRoutes: Routes = [
  {
    path:'',
    component:UserComponent,
    data: {
      breadcrumb: 'user'
    },
  },
  {
    path:'create',
    component:UserFormComponent,
    data: {
      breadcrumb: 'create'
    },
  }
];

@NgModule({
  imports: [RouterModule.forChild(UserRoutes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
