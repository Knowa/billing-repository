import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { UserFormComponent } from '../../shared/user-form/user-form.component';
import { User } from 'app/shared/models/user.model';
import { NgxSpinnerService } from 'ngx-spinner';
import { UserService } from '../../services/user.service';
import { ToastrService } from 'ngx-toastr';
import { finalize } from 'rxjs/operators';
import { DeletePopupComponent } from 'app/shared/components/delete-popup/delete-popup.component';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  user: User = new User();

  constructor(
    private router:Router,
    private modal: NgbModal,
    private spinner: NgxSpinnerService,
    private userService: UserService,
    private toastr: ToastrService

  ) { }

    userList:User[]=[];

  ngOnInit() {
    this.fetchUsers();
  }

  showUserPopupForm(mode,userId){
    if(userId.length > 0){
      this.user  = this.userList.find(usr => usr.userId === userId);
    }
    const user: User = mode === 'edit' ? this.user : new User();
    const modal: NgbModalRef = this.modal.open(UserFormComponent, { size: 'lg' , backdrop: 'static'});
    modal.componentInstance.mode = mode;
    modal.componentInstance.user = user;
    
    modal.result.then(
      data => {
        if (mode === 'add') {
          const sub = this.userService.addUser(data)
            .subscribe(
              res => {
                if (res.code === 200||res.code===201) {
                  this.toastr.success('User Successfully Created');
                  this.userList.unshift(res.result);
                }
                sub.unsubscribe();
              },
              err => {
                
                if(err.error.errors[0].defaultMessage){
                  this.toastr.error(err.error.errors[0].defaultMessage);
                }
                else{
                  this.toastr.error('Error creating user');
                }
                
                sub.unsubscribe();
              }
            );
        } else {
          const idx = this.userList.indexOf(this.user);
          const sub = this.userService.updateUser(data, this.user['userId'])
            .pipe(
              finalize(() => this.spinner.hide())
            )
            .subscribe(
              res => {
                if (res.code === 201 || res.code === 200) {
                  this.userList[idx] = res.result;
                  this.toastr.success('User details modified');
                  // this.userList = null;
                  sub.unsubscribe();
                }
              },
              err => {
                console.log(err);
                
                if(err.error.errors[0].defaultMessage){
                  this.toastr.error(err.error.errors[0].defaultMessage);
                }
                else{

                  this.toastr.error('Error modifying user details.');
                }
                sub.unsubscribe();
              }
            );
        }
      },
      cancel => { }
    );

  }

  fetchUsers(){
    this.spinner.show();
    this.userService.getUsers()
      .subscribe(
        data =>{
          console.log(data);
          
          this.userList = data.result;
          this.spinner.hide();

        },
        err =>{
          this.spinner.hide();
          this.toastr.error('Problem fetching Users');

          
        }
      )
  }

  delete(userId){
      this.user  = this.userList.find(usr => usr.userId === userId);
    
      const modal: NgbModalRef = this.modal.open(DeletePopupComponent);
  
      modal.result.then(
        yes => {
          const idx = this.userList.indexOf(this.user);
          const sub = this.userService.deleteUser(this.user.userId)
            .subscribe(
              data => {
                  this.userList[idx] = data.result;
                  this.toastr.success('User Successfully Deleted');
                  this.userList.splice(idx, 1);
                  this.user = null;
                  sub.unsubscribe();
              },
              err => {
                this.toastr.error('Error while removing the model.');
                sub.unsubscribe();
              }
            );
        },
        no => { }
      );
    }
  



}
