import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { UserRoutingModule } from './user-routing.module';
import { UserFormComponent } from './shared/user-form/user-form.component';
import { UserComponent } from './pages/user/user.component';
import {BreadcrumbModule} from 'angular-crumbs';
import { SharedModule } from 'app/shared/shared.module';
import { UserService } from './services/user.service';
import { DeletePopupComponent } from 'app/shared/components/delete-popup/delete-popup.component';



@NgModule({
  declarations: [UserFormComponent, UserComponent],
  imports: [
    SharedModule,
    CommonModule,
    UserRoutingModule,
    BreadcrumbModule,
    
    
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  entryComponents:[UserFormComponent,DeletePopupComponent],
  providers:[
    UserService
  ]
})
export class UserModule { }
