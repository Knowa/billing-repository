import { SidebarItem} from './models/sidebar-item.model';


export const SIDEBAR_LINKS: SidebarItem[] = [
  {
    label: 'ड्यास वोर्ड',
    link: 'home',
    icon: 'fa-sign-in-alt',
    hideTile: true,
    permissions: ['ALL']
  },  
  // {
  //   label: 'कार्यालय',
  //   link: 'municipality-office',
  //   icon: 'fa-building',
  //   permissions: ['ROLE_USER']
  // },
  {
    label: 'User',
    link: 'user',
    icon: 'fa-user',
    permissions: ['ROLE_ADMIN']
  },
  
  {
    label: 'Reports',
    link: 'reports',
    icon: 'far fa-file-alt',
    permissions: ['ROLE_ADMIN','ROLE_ACCOUNTANT']
    
  },

  {
    label: 'Add Items',
    link: 'addItems',
    icon: 'fas fa-cart-plus',
    permissions: ['ROLE_ACCOUNTANT']
    
  },

  {
    label: 'Select Items',
    link: 'selectItems',
    icon: 'fas fa-utensils',
    permissions: ['ROLE_ACCOUNTANT']
    
  },

  

  

];





