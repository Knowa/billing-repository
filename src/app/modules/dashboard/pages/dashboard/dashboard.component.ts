import { Component, OnInit, HostBinding, Input } from '@angular/core';
import { SidebarItem } from '../../models/sidebar-item.model';
import { SIDEBAR_LINKS } from '../../sidebar-links';

@Component({
  selector: 'lrms-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  
  @HostBinding('class')
  class = 'col p-0 d-flex flex-column';

  sidebarLinks: SidebarItem[] = SIDEBAR_LINKS;

  constructor(
  ) { }

  ngOnInit() {
  }

}