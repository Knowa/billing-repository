import { Component, OnInit, Input } from '@angular/core';
import { SidebarItem } from '../../models/sidebar-item.model';
import { SIDEBAR_LINKS } from '../../sidebar-links';
import { JwtHelperService } from '@auth0/angular-jwt';
import { User } from 'app/shared/models/user.model';
import { UserDetailService } from 'app/shared/services/user-detail.service';
// import { User } from '@shared/models/user.model';
// import { UserRole } from '@shared/models/userRole.model';
// import { UserDetailService } from '@shared/services/user-detail.service';

@Component({
  selector: 'lrms-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  sidebarItems: SidebarItem[] = SIDEBAR_LINKS;

  @Input()
  user: User = new User();

  constructor(
    private jwtHelper: JwtHelperService,
    private userDetailService: UserDetailService,
  ) {}
 

 ngOnInit() {
  //  this.getLoggedInUser();

  this.user=JSON.parse(localStorage.getItem('user'));

  //  console.log(this.sidebarItems, this.user);
   

 }

 

}
