import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { HomeComponent } from './pages/home/home.component';
import { UserRoutes } from '../user/user-routing.module';
import { ReportRoutes } from '../reports/reports-routing.module';
import { AddItemsRoutes } from '../add-items/add-items-routing.module';
import { SelectItemRoutes } from '../select-items/select-items-routing.module';
// import { TloRoutes } from '../tlo/tlo-routing.module';
// import { RoadRoutes } from '../road/road-routing.module';
// import { LandRoutes } from '../land/land-routing.module';
// import { HouseRoutes } from '../house/house-routing.module';
// import { FamilyRoutes } from '../family/family-routing.module';
// import { TaxRateRoutes } from '../tax-rate/tax-rate-routing.module';
// import { FeeChargeRoutes } from '../fee-charge/fee-charge-routing.module';
// import { PropertyRoutes } from '../property/property-routing.module';
// import { IndividualCardRoutes } from '../individual-card/individual-card-routing.module';
// import { TaxCalculationRoutes } from '../tax-calculation/tax-calculation-routing.module';
// import { UserRoutes } from '../user/user-routing.module';
// import { BusinessRegisterModule } from '../business-register/business-register.module';
// import { BusinessRegisterRoutes } from '../business-register/business-register-routing.module';
// import { MunicipalityOfficeRoutes } from '../municipality-office/municipality-office-routing.module';
// import { FacilityRoutes } from '../facilities/facilities-routing.module';
// import { FeeChargeCalculationRoutes } from '../fee-charge-calculation/fee-charge-calculation-routing.module';
// import { ReportRoutes } from '../reports/reports-routing.module';
// import { RoadSubBranchRoutes } from '../road-sub-branch/road-sub-branch-routing.module';
// import { TaxDetailRoutes } from '../tax-details/tax-details-routing.module';

export const DashboardRoutes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      {
        path: 'home',
        component: HomeComponent
      },
      {
        path: 'user',
        children:UserRoutes,
        data: 
        {
          breadcrumb: 'User'
        }
      
       
      },

      {
        path: 'reports',
        children: ReportRoutes,
        data: {
          breadcrumb: 'Report'
        }
      },

      {
        path: 'addItems',
        children: AddItemsRoutes,
        data: {
          breadcrumb: 'Add Items'
        }
      },

      {
        path: 'selectItems',
        children: SelectItemRoutes,
        data: {
          breadcrumb: 'Select Items'
        }
      },


      // {
      //   path: 'road',
      // // [userRole]:'ROLE_ADMIN',
      //   children: RoadRoutes,
      //   data: {
      //     breadcrumb: 'सडक'
      //   }
      // },

      //updated
      // {
      //   path: 'roadSubBranch',
      //   children: RoadSubBranchRoutes,
      //   // data: {
      //   //   breadcrumb: 'सडक'
      //   // }
      // },

      // {
      //   path: 'family',
      //   children: FamilyRoutes,
      //   data: {
      //     breadcrumb: 'परिवार'
      //   },
      //   //canActivate: [isAdminGuard, isNormalUserGuard, isModeratorGuard]
      // },

      // //updated
      // {
      //   path: 'tax-detail',
      //   children: TaxDetailRoutes,
      // },

      // {
      //   path: 'land',
      //   children: LandRoutes,
      //   data: {
      //     breadcrumb: 'जग्गा'
      //   }
      // },
      // {
      //   path: 'house',
      //   children: HouseRoutes,
      //   data: {
      //     breadcrumb: 'घर'
      //   }
      // },
      // {
      //   path: 'tax-rate',
      //   children: TaxRateRoutes,
      //   data: {
      //     breadcrumb: 'करको दर'
      //   }
      // },
      // {
      //   path:'fee-charge',
      //   children: FeeChargeRoutes,
      //   data: {
      //     breadcrumb: 'शुल्क दस्तुर'
      //   }        
      // },
      // {
      //   path: 'property',
      //   children: PropertyRoutes,
      //   data: {
      //     breadcrumb: 'सम्पति'
      //   }        
      // },
      // {
      //   path:'tax-calculation',
      //   children: TaxCalculationRoutes,
      //   data: {
      //     breadcrumb: 'कर निर्धारण'
      //   }
      // },
      // {
      //   path:'fee-charge-calculation',
      //   children: FeeChargeCalculationRoutes,
      //   data: {
      //     breadcrumb: 'शुल्क दस्तुर निर्धारण'
      //   }        
      // },
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
      },

      
    ]
  }
];

// const routes: Routes = [];

// @NgModule({
//   imports: [RouterModule.forChild(routes)],
//   exports: [RouterModule]
// })
// export class DashboardRoutingModule { }
