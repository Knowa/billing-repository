import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { NavbarComponent } from './shared/components/navbar/navbar.component';
import { SharedModule } from '../../shared/shared.module';
import { UserProfileDropdownComponent } from './shared/components/user-profile-dropdown/user-profile-dropdown.component';
import { SidebarComponent } from './shared/components/sidebar/sidebar.component';
import { HomeComponent }   from './pages/home/home.component';
import { SidebarFilterPipe } from './shared/pipes/sidebar-filter.pipe';


@NgModule({
  imports: [
    CommonModule,
    SharedModule
    // DashboardRoutingModule
  ],
  declarations: [DashboardComponent, NavbarComponent, UserProfileDropdownComponent, SidebarComponent, HomeComponent, SidebarFilterPipe]
})
export class DashboardModule { }
