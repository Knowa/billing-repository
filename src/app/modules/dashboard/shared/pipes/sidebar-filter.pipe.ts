import { Pipe, PipeTransform } from '@angular/core';
import { SidebarItem } from '../../models/sidebar-item.model';
// import { User } from '@shared/models/user.model';
// import { UserService } from '../../../user/services/user.service';
// import { TokenService } from '@shared/tokenServices/token.service';

@Pipe({
  name: 'sidebarFilter'
})
export class SidebarFilterPipe implements PipeTransform {

  transform(sidebarItem: SidebarItem[], role?: string): SidebarItem[] {
    const filteredLinks = sidebarItem.filter(link => {
    return link.permissions.includes('ALL') || link.permissions.includes(role);
    });

    return filteredLinks;
  }

}
