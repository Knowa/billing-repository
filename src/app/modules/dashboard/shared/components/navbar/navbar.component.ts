import { Component, OnInit, Input } from '@angular/core';
// import { SpinnerService } from '@shared/modules/spinner/spinner.service';
// import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';

@Component({
  selector: 'bill-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  isNavbarCollapsed = false;

  todayDate;
  

  constructor(
    // private spinner: SpinnerService,
    // private municipalityService: MunicipalityOfficeService,
    // private taxCalculationService: TaxCalculationService
  ) { }

  subscription: Subscription

  ngOnInit() {
  }

}
