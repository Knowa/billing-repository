import { Component, OnInit, HostBinding, Input } from '@angular/core';
import { SidebarItem } from '../../../models/sidebar-item.model';
import { JwtHelperService } from '@auth0/angular-jwt';
import { User } from 'app/shared/models/user.model';
import { UserRole } from 'app/shared/models/userRole.model';
import { UserDetailService } from 'app/shared/services/user-detail.service';
// import { UserService } from 'src/app/modules/user/services/user.service';

@Component({
  selector: 'lrms-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  @Input()
  sidebarItems: SidebarItem[] = [];

  user:User;

  userRole: UserRole;

  //sidebar links for section-user
  // sidebarItemsSectionUser: SidebarItem[] = [
  //   {
  //     label: 'ड्यास वोर्ड',
  //     link: 'home',
  //     icon: 'fa-sign-in-alt',
  //     hideTile: true
  //   },
  //   {
  //     label: 'कार्यालय',
  //     link: 'municipality-office',
  //     icon: 'fa-building',
  //   },
  //   {
  //     label: 'टोलविकास संस्था',
  //     link: 'tlo',
  //     icon: 'fa-map'
  //   },
  //   {
  //     label: 'सडक/ घरमा रहेको सेवा',
  //     link: 'facilities',
  //     icon: 'fas fa-clone'
  //   },
  //   {
  //     label: 'सडकको नाम / विवरण',
  //     link: 'road',
  //     icon: 'fa-road'
  //   },
  //   {
  //     label: 'पारिवारिक विवरण',
  //     link: 'family',
  //     icon: 'fa-users'
  //   },
  //   {
  //     label: 'जग्गाको न्यूनतम मूल्य',
  //     link: 'land',
  //     icon: 'fa-th-large'
  //   },
  //   {
  //     label: 'घरको न्यूनतम मूल्य',
  //     link: 'house',
  //     icon: 'fa-home'
  //   },
  //   {
  //     label: 'करको दर',
  //     link: 'tax-rate',
  //     icon: 'fa-book'
  //   },
    
  //   {
  //     label: 'शुल्क दस्तुर दर',
  //     link: 'fee-charge',
  //     icon: 'fas fa-tag'
  //   },
  //   {
  //     label: 'व्यक्तिको सम्पति विवरण',
  //     link: 'property',
  //     icon: 'fas fa-money-bill-alt'
  //   },
  //   {
  //     label: 'कर निर्धारण / असूली',
  //     link: 'tax-calculation',
  //     icon: 'fas fa-calculator'
  //   },
  //   {
  //     label: 'शुल्क दस्तुर असूली',
  //     link: 'fee-charge-calculation',
  //     icon: 'fas fa-calculator'
  //   }
  // ];


  //sidebar links for collection-user
  // sidebarItemsCollectionUser: SidebarItem[] = [
  //   {
  //     label: 'ड्यास वोर्ड',
  //     link: 'home',
  //     icon: 'fa-sign-in-alt',
  //     hideTile: true
  //   },
  //   {
  //     label: 'कर निर्धारण / असूली',
  //     link: 'tax-calculation',
  //     icon: 'fas fa-calculator'
  //   },
  //   {
  //     label: 'शुल्क दस्तुर असूली',
  //     link: 'fee-charge-calculation',
  //     icon: 'fas fa-calculator'
  //   }
  // ];


  //sidebar links for data-entry-user
  // sidebarItemsDataEntryUser: SidebarItem[] = [
  //   {
  //     label: 'ड्यास वोर्ड',
  //     link: 'home',
  //     icon: 'fa-sign-in-alt',
  //     hideTile: true
  //   },
  //   {
  //     label: 'कार्यालय',
  //     link: 'municipality-office',
  //     icon: 'fa-building',
  //   },
  //   {
  //     label: 'प्रयोग कर्ता',
  //     link: 'user',
  //     icon: 'fa-user'
  //   },
  //   {
  //     label: 'टोलविकास संस्था',
  //     link: 'tlo',
  //     icon: 'fa-map'
  //   },
  //   {
  //     label: 'सडक/ घरमा रहेको सेवा',
  //     link: 'facilities',
  //     icon: 'fas fa-clone'
  //   },
  //   {
  //     label: 'सडकको नाम / विवरण',
  //     link: 'road',
  //     icon: 'fa-road'
  //   },
  //   {
  //     label: 'पारिवारिक विवरण',
  //     link: 'family',
  //     icon: 'fa-users'
  //   },
  //   {
  //     label: 'जग्गाको न्यूनतम मूल्य',
  //     link: 'land',
  //     icon: 'fa-th-large'
  //   },
  //   {
  //     label: 'घरको न्यूनतम मूल्य',
  //     link: 'house',
  //     icon: 'fa-home'
  //   },
  //   {
  //     label: 'करको दर',
  //     link: 'tax-rate',
  //     icon: 'fa-book'
  //   },
  //   {
  //     label: 'शुल्क दस्तुर दर',
  //     link: 'fee-charge',
  //     icon: 'fas fa-tag'
  //   },
  //   {
  //     label: 'व्यक्तिको सम्पति विवरण',
  //     link: 'property',
  //     icon: 'fas fa-money-bill-alt'
  //   },
  // ];

  @HostBinding('class')
  classes = 'col p-0 d-flex flex-column';

  constructor(
    private userDetailService: UserDetailService,
    private jwtHelper : JwtHelperService
  ) {
    
  }
  
  ngOnInit() {
    this.getLoggedInUser();
  }


  getLoggedInUser(){
    this.userDetailService.getUser()
      .subscribe(
        user => {
          this.user = user;
        }
    );
  }

}