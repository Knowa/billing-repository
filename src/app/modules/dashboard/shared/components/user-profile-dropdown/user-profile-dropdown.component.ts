import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserDetailService } from 'app/shared/services/user-detail.service';
import { BillConfirmComponent } from 'app/modules/select-items/pages/bill-confirm/bill-confirm.component';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
// import { LoginService } from 'src/app/modules/login/services/login.service';
// import { UserDetailService } from '@shared/services/user-detail.service';

@Component({
  selector: 'bill-user-profile-dropdown',
  templateUrl: './user-profile-dropdown.component.html',
  styleUrls: ['./user-profile-dropdown.component.scss']
})
export class UserProfileDropdownComponent implements OnInit {

  constructor(
    private router: Router,
    private userDetailService: UserDetailService,
    private modal: NgbModal,
  ) { }

  ngOnInit() {
  }

  logout() {
    localStorage.removeItem('access_token');
    // localStorage.removeItem('user');
    this.userDetailService.setUser(null);
    this.router.navigate(['/login']);
  }


  openProfileSeeting(){
    const modal: NgbModalRef = this.modal.open(BillConfirmComponent, { size: 'sm' , backdrop: 'static'});
  }

}
