export interface SidebarItem {
  label?: string;
  link?: string;
  icon?: string;
  hideTile?: boolean;
  permissions?: string[];
}

