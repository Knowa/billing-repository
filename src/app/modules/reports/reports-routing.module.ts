import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReportComponent } from './shared/report/report.component';


export const ReportRoutes: Routes = [
  {
    path:'',
    component:ReportComponent

  }
];

@NgModule({
  imports: [RouterModule.forChild(ReportRoutes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule { }
