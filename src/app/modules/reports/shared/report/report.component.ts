import { Component, OnInit } from '@angular/core';
import * as jsPDF from 'jspdf'
import 'jspdf-autotable';
import { UserDetailService } from 'app/shared/services/user-detail.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { User } from 'app/shared/models/user.model';


@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {

  duration='';
  fromDate: Date;
  toDate: Date;

  user:User

  generateReport=false;

  itemList=[
    {
      'sn':1,
      'item':'Beer',
      'quantity':'3',
      'price':'750'
    }, {
      'sn':2,
      'item':'tequilla',
      'quantity':'3',
      'price':'2500'
    },
    {
      'sn':3,
      'item':'mojito',
      'quantity':'1',
      'price':'750'
    },

  ];
  constructor(
    private userDetailService: UserDetailService,
    private jwtHelper : JwtHelperService
  ) {
    
  }
  
  ngOnInit() {
    // this.getLoggedInUser();
    this.user=JSON.parse(localStorage.getItem('user'));

  }


  getLoggedInUser(){
    this.userDetailService.getUser()
      .subscribe(
        user => {
          this.user = user;
        }
    );
  }

  fetchReportDetail(){
    this.generateReport = true;
    let fromD = new Date(this.fromDate);
    let toD = new Date(this.toDate);
    console.log(this.duration, fromD.getTime(), toD.getTime());
  }

  generatePdf() {
    var doc = new jsPDF();
    var rows = [];

    this.itemList.forEach(item => {
      var tempSpeed = [
       item.sn,
       item.item,
       item.quantity,
       item.price
      ];
      rows.push(tempSpeed);
    })

    doc.setFontSize(12);

    //first and second are the x,y coordinates of the text to be positioned fron left corner
    doc.text(15, 10, 'Daily Invoice Report');
    // doc.text(75, 10, `Tracker : ${this.trackerName}`);

    doc.autoTable({
      head: [['S.N','Item','Quantity','Total Price']],
      body: rows,
      columnStyles: {
        0: {columnWidth: 40},
        1: {columnWidth: 40},
        2: {columnWidth: 40},
        3: {columnWidth: 40},
      },

      // customize table header and rows format
      theme: 'striped'
    });
    
    doc.save('daily-Invoice-Report.pdf')
  }




}
