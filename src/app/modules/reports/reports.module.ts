
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportsRoutingModule } from './reports-routing.module';
import { ReportComponent } from './shared/report/report.component';
import { SharedModule } from 'app/shared/shared.module';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [ReportComponent],
  imports: [
    CommonModule,
    ReportsRoutingModule,
    SharedModule,
    FormsModule
  ]
})
export class ReportsModule { }


