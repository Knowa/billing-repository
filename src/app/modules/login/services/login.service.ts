import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// import { User } from '@shared/models/user.model';
import { BehaviorSubject, Observable } from 'rxjs';
// import { HttpResult } from '@shared/models/http-result.model';
import { JwtHelperService } from '@auth0/angular-jwt';
// import 'rxjs/add/operator/catch';
import { User } from 'app/shared/models/user.model';

@Injectable({
  providedIn: 'root'
})

export class LoginService {

  public user: User;
  
  private user$: BehaviorSubject<User> = new BehaviorSubject(null);  

  constructor(
    private http: HttpClient,
    private jwtHelper : JwtHelperService
  ) { }


  login(username, password): Observable<any>  {
    console.log('in user servce' ,username, password);
    
    return (
      this.http
        .post<User>('/login', {username, password})
        // .catch( err => {
        //   return Observable.throw(err.error);
        // })
    );
  }

  
  // setUser(user: User): void {
  //   this.user = user;
  //   this.user$.next(user);
  // }

  // getUser(): Observable<User> {
  //   if(!this.user) {
  //     this.fetchUser()
  //       .subscribe(
  //         data => {
  //           this.setUser(data.result);
  //           console.log('the get user is ', this.getUser());
            
  //         },
  //         error => {
  //         }
  //       );
  //   }
  //   return this.user$.asObservable();
  // }

  // private fetchUser(): Observable<HttpResult> {
  //   const username  = this.jwtHelper.decodeToken().sub;
  //   return this.http.get<HttpResult>('/users/findUserRole/'+username);
  // }  
}
