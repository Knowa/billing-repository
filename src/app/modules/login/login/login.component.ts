import { Component, OnInit, HostBinding, Input } from '@angular/core';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { UserDetailService } from 'app/shared/services/user-detail.service';
import { Subscription } from 'rxjs';
import { LoginService } from '../services/login.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  username = '';
  password = '';
  showPassword= false;
  loginLabel ='Sign In';
  signingIn = false;
  
  @HostBinding('class')
  class = 'col p-0 d-flex flex-column';

  constructor(
    private loginService: LoginService,
    private router: Router,
    private jwtHelper: JwtHelperService,
    private userDetailService: UserDetailService,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
  ) { }


  subscribtion: Subscription


  ngOnInit() {
  }

  ngOnDestroy() {
    if (this.subscribtion) {
      this.subscribtion.unsubscribe();
    }
  }

  login(){
    this.signingIn = true;
    this.loginLabel="Signing In ...";
    this.loginService.login(this.username, this.password)
      .subscribe(
        (data: any) => {
          this.signingIn = false;
          localStorage.setItem('access_token','Basic '+data.meta)
          localStorage.setItem('user',JSON.stringify(data.result))
          this.router.navigate(['/dashboard']);
        },
        error =>{
          this.signingIn = false;
          this.loginLabel ='Sign In';
          this.toastr.error('Problem Logging in');
          console.error('error in login',error);
        }
      );
  }


}
