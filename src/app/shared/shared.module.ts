import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpInterceptorService } from './services/http-interceptor.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthGuard } from './guards/auth.guard';
import {BreadcrumbModule} from 'angular-crumbs';
import { BreadcrumbComponent } from './components/breadcrumb/breadcrumb.component';
import { UserModule } from 'app/modules/user/user.module';
import { ToastrModule } from 'ngx-toastr';
import { SaveCancelButtonsComponent } from './save-cancel-buttons/save-cancel-buttons.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { LoginSpinnerComponent } from './login-spinner/login-spinner.component';
import { RequiredClassComponent } from './components/required-class/required-class.component';
import { DeletePopupComponent } from './components/delete-popup/delete-popup.component';




@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    RouterModule,
    BrowserAnimationsModule,
    BreadcrumbModule,
    NgxSpinnerModule,
    ToastrModule.forRoot({
      timeOut: 5000,
      positionClass: 'toast-top-right',
      preventDuplicates: true,
      autoDismiss: false,
      closeButton: true
    }),
  ],

  declarations: [
    BreadcrumbComponent,
    SaveCancelButtonsComponent,
    LoginSpinnerComponent,
    RequiredClassComponent,
    RequiredClassComponent,
    DeletePopupComponent
    ],

  exports: [
    NgbModule,
    FormsModule,
    RouterModule,
    BreadcrumbModule,
    BreadcrumbComponent,
    SaveCancelButtonsComponent,
    LoginSpinnerComponent,
    RequiredClassComponent,
    DeletePopupComponent

  ],
  providers: [
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true
    },
  ],
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule
    };
  }
}
