import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable()
export class HttpInterceptorService implements HttpInterceptor {

  constructor() { }

  
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (
      req.params.has('interceptor') &&
      req.params.get('interceptor') === 'skip'
    ) {
      const params = req.params.delete('interceptor');
      return next.handle(req.clone({ params }));
    }

    let alteredReq = req.clone({
      url: environment.apiUrl + req.url,
    });
    const headers = req.headers;
    const token = localStorage.getItem('access_token');
    const header = !req.headers.get('Authorization');
    if (token && header) {
      headers.append('Authorization', token);
      alteredReq = alteredReq.clone({
        setHeaders: {
          Authorization: token,
        },
      });
    }
    return next.handle(alteredReq)
    //   evt => {},
    //   err => {
    //     if (err instanceof HttpErrorResponse) {
    //     }
    //   }
    // );
  }
}
