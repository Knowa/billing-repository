import { Injectable } from '@angular/core';
// import { User } from '@shared/models/user.model';
// import { Observable } from 'rxjs/Observable';
// import { BehaviorSubject } from 'rxjs/BehaviorSubject';
// import { HttpResult } from '@shared/models/http-result.model';
import { HttpClient } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';
import { User } from '../models/user.model';
import { HttpResult } from '../models/http-result.model';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserDetailService {

  public user: User;

  private user$: BehaviorSubject<User> = new BehaviorSubject(null);  
  
  constructor(
    private http: HttpClient,
    private jwtHelper : JwtHelperService
  ) { }

  setUser(user: User): void {
    this.user = user;
    this.user$.next(user);
  }

  getUser(): Observable<User> {
    if(!this.user) {
      this.fetchCurrentUser()
        .subscribe(
          data => {
            this.setUser(data.result);
          },
          error => {
          }
        );
    }
    return this.user$.asObservable();
  }

  public fetchCurrentUser(): Observable<HttpResult> {
    return this.http.get<HttpResult>('/user/profile');
  } 
}
