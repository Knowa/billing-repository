import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'login-spinner',
  templateUrl: './login-spinner.component.html',
  styleUrls: ['./login-spinner.component.scss']
})
export class LoginSpinnerComponent implements OnInit {
  
  @Input()
  color = 'mixed';

  @Input()
  size = 'md';

  constructor() { }

  ngOnInit() {
  }

}
