export class UserRole {
  
  userRoleId: number;
  userRoleName: string;
  userRoleDateCreated?: Date;
  userRoleDateModified?: Date;

  constructor() {
      
  }
}