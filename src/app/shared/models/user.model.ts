import { UserRole } from './userRole.model';

export class User {

  userId: string;
  firstName: string;
  middleName?: string;
  lastName: string;
  username: string;
  password: string;
  address?: string;
  gender: string;
  role: string;
  status: string;
  contactNumber?: string;

  userEmail: string;
  enabled : boolean;

  result: any;


  constructor() {
    this.gender='MALE';

  }

}