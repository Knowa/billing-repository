export class FoodItem {
  name: string;
  price: number;
  quantity: number;
  imageUrl: string;

  totalPrice: number;
  billPrice: number;


  customerName: string;


  // foodItemList: FoodItem[] = [];


  constructor() {
    this.quantity = 1;
    // this.foodItemList = [];
  }
}