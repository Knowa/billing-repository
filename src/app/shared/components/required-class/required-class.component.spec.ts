import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequiredClassComponent } from './required-class.component';

describe('RequiredClassComponent', () => {
  let component: RequiredClassComponent;
  let fixture: ComponentFixture<RequiredClassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequiredClassComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequiredClassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
