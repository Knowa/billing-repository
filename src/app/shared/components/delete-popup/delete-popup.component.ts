import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'billing-delete-popup',
  templateUrl: './delete-popup.component.html',
  styleUrls: ['./delete-popup.component.scss']
})
export class DeletePopupComponent implements OnInit {

  title = 'Confirm Delete';

  message = 'Are you sure you want to remove this record?';

  constructor(
    private modal: NgbActiveModal
  ) { }

  close() {
    this.modal.close();
  }

  dismiss() {
    this.modal.dismiss();
  }

  ngOnInit() {
  }

}
